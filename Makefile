all:
	python server.py

package:
	rm -f ../symantec-users.tar
	git archive --format=tar --prefix=symantec-users/ -o ../symantec-users.tar HEAD
	tar tf ../symantec-users.tar


.PHONY: all package