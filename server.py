import os
import re
import logging
import json
import functools 
import bcrypt
import datetime
import string
import collections
import base64
import subprocess

import tornado.ioloop
import tornado.web

logging_level = os.getenv("USERS_LOGGING_LEVEL", "DEBUG")
logging.basicConfig(
  level=logging_level,
  format='%(asctime)s  %(levelname)7s  %(message)s'
)

### Bootstrap creds
BOOTSTRAP_USERNAME = "bootstrap"
BOOTSTRAP_PASSWORD = "zkesBHRj7Yd99VUGdbjnazMQgPkcmEYBo4UJpUQhWHogMJQWNBBkhgzEooyncdJc"

### Denied permissions:
### - to delete one's self
### - to update one's self role)
###
### This is to prevent an admin to lock iself out of permissions and users from
### escalate their own permissions. Roles can only be assigned by someone else.
### Users can only be deleted by someone else.
PERMISSION_CREATE      = "create"
PERMISSION_READ_SELF   = "read-self"
PERMISSION_READ_ALL    = "read-all"
PERMISSION_UPDATE_SELF = "update-self"
PERMISSION_UPDATE_ROLE = "update-role"
PERMISSION_UPDATE_ALL  = "update-all"
PERMISSION_DELETE_ALL  = "delete-all"

### Roles groups permissions
ROLES = {
    "admin": [
        PERMISSION_CREATE,
        PERMISSION_READ_SELF,
        PERMISSION_UPDATE_SELF,

        PERMISSION_READ_ALL,
        PERMISSION_UPDATE_ALL,
        PERMISSION_UPDATE_ROLE,

        PERMISSION_DELETE_ALL,
    ],
    "staff": [
        PERMISSION_READ_SELF,
        PERMISSION_UPDATE_SELF,

        PERMISSION_READ_ALL,
        PERMISSION_UPDATE_ALL,
    ],
    "user": [
        PERMISSION_READ_SELF,
        PERMISSION_UPDATE_SELF,
    ],
}


class PasswordHelper(object):
    """
    A simple static class to help with password hashing and checks
    """

    @staticmethod
    def hash_password(password):
        logging.debug("==> Hashing password %s" % password)
        hashed = bcrypt.hashpw(password, bcrypt.gensalt())
        return hashed

    @staticmethod
    def check_password(password, hashed):
        logging.debug("==> Checking password %s vs hashed %s" % (password, hashed))
        return bcrypt.hashpw(
            password.encode("UTF-8"), hashed.encode("UTF-8")
        ) == hashed


class FileDriver(object):
    """
    A simple class that reads and write json records to disk.
    """

    FILE_PATH = "users.json"
    records = {}

    def __init__(self):
        self.read()

    def write(self):
        logging.info("==> Writting records to file %s" % self.FILE_PATH)
        try:
            # with open(self.FILE_PATH, "w", encoding="utf-8") as f:
            with open(self.FILE_PATH, "w") as f:
                json.dump(self.records, f, indent = 2)
        except BaseException as e:
            logging.error(e)


    def read(self):
        logging.info("==> Reading records from file %s." % self.FILE_PATH)

        try:
            with open(self.FILE_PATH, "r") as f:
                self.records = json.load(f)
        except BaseException as e:
            logging.warn("==> Failed to read file: %s." % e)
            self.records = {}

        logging.info("==> Read %i record(s) from file." % len(self.records))


class DataManager(FileDriver):
    """
    Extends the FileDriver class to handle CRUD access methods to the data.

    Note that access control checks are not done at this point but instead by
    the BaseHandler. This is because that handler needs to access the details
    of a user before it can decide to proceed with the request or not.
    """

    def is_empty(self):
        """
        This method checks if our dataset is empty.

        When the dataset is empty, we can't perform access controls checks
        _until_the very first record gets written. This first record also needs
        to gain "admin" role access to be able to create and edit all others
        users.
        """
        return len(self.records) == 0


    def create(self, data):
        """
        Creates a new user.

        We generate a new UUID each time a new user is created. This avoids
        sequence number attackcs. Version 1 avoids collisions better than
        version 4.
        """
        logging.info("==> Creating a new record")
        valid = DataHelper.check_data(data, True)
        logging.debug("==> CREATE data %s" % data)
        logging.debug("==> CREATE valid %s" % valid)

        name = data["name"]
        data["created"] = str(datetime.datetime.now())
        data["updated"] = None

        if self.is_empty:
            data["role"] = "admin"

        self.records[name] = data
        self.write()

        logging.info("==> Created a new record")
        return data


    def get_all(self):
        logging.info("==> Getting all records")
        return self.records


    def get_one(self, name):
        logging.info("==> Getting record %s" % name)
        return self.records.get(name, None)


    def delete(self, name):
        logging.info("==> Deleting record %s" % name)
        original = self.get_one(name)
        if not original:
            raise UserNotFoundError("Record %s cannot be found." % name)

        del self.records[name]
        self.write()


    def update(self, name, data):
        """
        Updates an existing record with a new one.

        Because Tornado doesn't offer a PATCH http method we use POST and PUT as
        a PATCH too. This means, we first fetch the original method and then
        merge it with the data to be updated and store that instead.
        """

        logging.info("==> Updating record %s" % name)

        valid = DataHelper.check_data(data, False)
        merge = self.get_one(name)
        if not merge:
            raise UserNotFoundError("Record %s cannot be found." % name)

        DataHelper.dict_merge(merge, valid)

        merge["updated"] = str(datetime.datetime.now())
        self.records[name] = merge
        logging.info("==> ALL %s" % self.records)
        self.write()

        logging.info("==> Updated the record %s" % name)
        return merge


class DataHelper(object):
    """
    This static class helps to check data sanity before commiting to the file
    database.
    """

    @staticmethod
    def dict_merge(original, replace):
        """
        From https://gist.github.com/angstwad/bf22d1822c38a92ec0a9

        Recursive dict merge. Inspired by :meth:``dict.update()``, instead of
        updating only top-level keys, dict_merge recurses down into dicts nested
        to an arbitrary depth, updating keys. The ``replace`` is merged into
        ``original``.
        """
        for k, v in replace.iteritems():
            if (k in original and isinstance(original[k], dict)
                    and isinstance(replace[k], collections.Mapping)):
                dict_merge(original[k], replace[k])
            else:
                original[k] = replace[k]

    @staticmethod
    def check_required_fields(data):
        """
        Checks that all required fields are present when creating or updating a
        record.
        """
        required_fields = [
            "name",
            "password",
            "role",
        ]

        for field in required_fields:
            if not field in data:
                raise ValueError("Required fields are: %s" % required_fields)


    @staticmethod
    def check_allowed_fields(data, is_create):
        """
        Checks that only certain fields are allowed when creating or updating a
        record.

        Any other fields results in a request error.
        """
        allowed_fields = [
            "name",
            "password",
            "role",

            "email",
            "phone_number",
            "address",
            "notes",
        ]

        forbidden_fields = [
            "created",
            "updated",
        ]

        if not is_create:
            forbidden_fields.append("name")

        logging.debug("==> Allowed fields %s" % allowed_fields)
        logging.debug("==> Forbidden fields %s" % forbidden_fields)

        for field in data:
            if not field in allowed_fields:
                raise ValueError("Field '%s' is not allowed to update." % field)

            if field in forbidden_fields:
                raise ValueError("Field '%s' is forbidden to update." % field)


    @staticmethod
    def custom_check_name(value):
        """
        Custom check method to ensure user names are unique.
        """
        logging.debug("==> Checking name %s" % value)
        value = string.lower(value)
        regex = r"[a-z0-9][a-z0-9\-]*"
        match = re.match(regex, value)
        if not match:
            raise ValueError(
                "Usernames accept only alphanumeric characters and hyphens."
            )

        if value in manager.get_all():
            raise ValueError("Username %s already exists.")

        return value


    @staticmethod
    def custom_check_password(value):
        """
        Custom check method to ensure password have the minimum strength to be
        considered acceptable.
        """
        logging.debug("==> Checking password %s" % value)

        msg  = "Passwords need to be 10 char long and have at least: "
        msg += "an lower case char, an upper case char, "
        msg += "a digit and a symbol (%s)." % string.punctuation

        value = str(value)
        if len(value) < 10:
            raise ValueError(msg)

        has_lower  = False
        has_upper  = False
        has_digit  = False
        has_symbol = False

        for char in value:
            has_lower  = has_lower  or (string.ascii_lowercase.find(char) > -1)
            has_upper  = has_upper  or (string.ascii_uppercase.find(char) > -1)
            has_digit  = has_digit  or (string.digits.find(char) > -1)
            has_symbol = has_symbol or (string.punctuation.find(char) > -1)

        if not (has_lower and has_upper and has_symbol and has_digit):
            raise ValueError(msg)

        ### Return the hashed value, NOT the original one!
        return PasswordHelper.hash_password(value)


    @staticmethod
    def custom_check_email(value):
        """
        Custom check method to ensure emails look valid.
        """
        logging.debug("==> Checking email %s" % value)
        regex = r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$"
        if not re.match(regex, str(value)):
            raise ValueError("Email needs to match the expression %s" % regex)

        return value


    @staticmethod
    def check_data(data, is_create):
        DataHelper.check_allowed_fields(data, is_create)
        if is_create:
            DataHelper.check_required_fields(data)

        for field, value in data.iteritems():
            logging.debug("==> Checking field %s" % field)

            try:
                func = getattr(DataHelper, "custom_check_" + field)
                logging.debug("==> Running custom check for field %s" % field)
                value = func(value)
                ### Reassign the value returned from the custom check back. This
                ### means custom checks can manipulate data as they see fit.
                logging.debug("==> Writting field %s: %s" % (field, value))
                data[field] = value

            except AttributeError as e:
                logging.debug("==> No custom check for field %s" % field)

        return data


class UserNotFoundError(Exception):
    pass


class AuthenticationError(Exception):
    pass


class AuthorizationError(Exception):
    pass


class BaseHandler(tornado.web.RequestHandler):
    current_user = None
    current_name = None
    current_role = None

    def prepare(self):
        logging.info("")
        logging.info("==> Preparing request")
        self.authenticate()


    def extract_auth(self):
        logging.debug("==> Checking Authentication header")
        header  = self.request.headers.get("Authorization", None)
        if not header:
            raise AuthenticationError(
                "Failed to find Authentication header."
            )

        logging.debug("==> Authentication header found: %s" % header)
        encoded = header.replace("Basic ", "")
        logging.debug("==> Encoded value found: %s" % encoded)
        decoded = base64.b64decode(encoded)
        username, password = decoded.split(":")
        logging.debug("==> User name found %s" % username)

        return username, password


    def authenticate(self):
        try:
            if manager.is_empty():
                username, password = self.extract_auth()
                if username == BOOTSTRAP_USERNAME \
                    and password == BOOTSTRAP_PASSWORD:
                    logging.info("Bootstrap creds check.")
                    return

                else:
                    logging.info("Bootstrap: %s %s" % (username, password))
                    raise AuthenticationError("Bootstrap creds are wrong.")

            username, password = self.extract_auth()
            user = manager.get_one(username)
            if not user:
                raise UserNotFoundError("User %s cannot be found." % username)

            logging.debug("==> User found %s" % user)
            hashed = user["password"]
            if not PasswordHelper.check_password(password, hashed):
                logging.debug("==> Failed with password %s vs %s." %
                    (password, hashed)
                )
                raise AuthenticationError(
                    "Password failed to match for user %s." % username
                )

            logging.info("==> Authenticated as username %s." % username)

            ### Cache values for later use
            self.current_user = user
            self.current_name = user["name"]
            self.current_role = user["role"]

        except Exception as e:
            logging.error(e)
            error = str(e)

            self.send("authenticate", None, None, error, 401)
            self.finish()


    def authorize(self, permission):
        if manager.is_empty():
            return

        role   = ROLES.get(self.current_role, None)
        action = "authorize-%s" % permission
        name   = self.current_name

        if not role:
            self.send(action, name, None, "Action denided", 403)
            self.finish()

        if not permission in role:
            self.send(action, name, None, "Action denided", 403)
            self.finish()


    def remove_sensitive_fields(self, data):
        """
        Remove sensitive fields from data returned
        """

        if not data:
            return

        sensitive_fields = [
            "password",
        ]

        clean = {}
        for field in data:
            if not field in sensitive_fields:
                clean[field] = data[field]

        return clean


    def send(self, action, name, data, error=None, status=200):
        clean = self.remove_sensitive_fields(data)
        logging.debug("==> About to send " + 
            "%s %s: %s, %s ==> %s" %(action, name, clean, error, status))

        res = {
            "ok": error is None,
            "action": action,
        }

        if name:
            res["name"] = name

        if error:
            res["error"] = error
        elif data:
            res["data"] = clean

        self.set_status(status)
        self.write(res)


class VersionHandler(tornado.web.RequestHandler):
    """
    Unathenticated endpoint just to check the app is up (useful for monitoring)
    """

    def get(self):
        cmd_commit  = ["git", "log", "-1", "--pretty=format:%H"]
        cmd_date    = ["git", "log", "-1", "--pretty=format:%aD"]
        cmd_version = ["git", "describe", "--dirty"]

        try:
            self.write({
                "name": "symantec-users",
                "version": subprocess.check_output(cmd_version).strip(),
                "commit": subprocess.check_output(cmd_commit),
                "date"  : subprocess.check_output(cmd_date),
            })

        except BaseException:
            self.write({
                "name": "symantec-users",
                "version": "0.0.1",
            })


# class Custom404Handler(tornado.web.RequestHandler):
class Custom404Handler(BaseHandler):
    """
    Custom 404 handler that sends json instead of HTML.
    """

    def get(self):
        self.set_status(404)
        self.write({
            "ok": False,
            "error": "Resource not found.",
            "uri": self.request.uri,
        })


class MeHandler(BaseHandler):
    def get(self):
        """
        Convinience endpoint to get one's self record and to test a successful
        authentication.
        """
        self.authorize(PERMISSION_READ_SELF)
        data = manager.get_one(self.current_name)
        self.send("me", self.current_name, data)


class UsersHandler(BaseHandler):
    """
    The main handler.
    """

    def get(self, name):
        action = "read"
        data   = None
        error  = None
        status = 200

        try:
            if not name:
                action = "read-all"
                self.authorize(PERMISSION_READ_ALL)
                users = manager.get_all()
                data  = {}
                for username, user in users.iteritems():
                    data[username] = self.remove_sensitive_fields(user)
                logging.debug("Data all: %s" % data)

            elif name == self.current_name:
                action = "read-self"
                self.authorize(PERMISSION_READ_SELF)
                data = manager.get_one(name)

            else:
                action = "read-other"
                self.authorize(PERMISSION_READ_ALL)
                data = manager.get_one(name)

        except Exception as e:
            error  = str(e)
            status = 400

        self.send(action, name, data, error, status)

    def delete(self, name):
        action = "delete"
        error  = None
        # Per HTTP protocol the correct way to return to a delete is to
        # issue a 204 status code when all goes well. But for API
        # consistency we wrap all actions into a envelope with metadata.
        # self.set_status(204, "Record %s deleted" % name)
        status = 200

        logging.debug("DELETE name %s" % name)
        logging.debug("DELETE current_name %s" % self.current_name)

        if not name:
            error = "User name is required for delete."
            status = 400

        elif name == self.current_name:
            action = "delete-self"
            error  = "Unfortunately, you are not authorized to commit suicide. "
            error += "Go for a walk or talk to someone on 81533300 instead."
            status = 403

        else:
            user = manager.get_one(name)
            if not user:
                error = "User name %s not found." % name
                status = 404
            else:
                self.authorize(PERMISSION_DELETE_ALL)
                manager.delete(name)

        self.send(action, name, None, error, status)


    def post(self, name):
        action = None
        user   = None
        error  = None
        status = 200

        try:
            data = json.loads(self.request.body)
            logging.debug("==> POST data %s" % data)
            logging.debug("==> POST name %s" % name)

            if not name:
                self.authorize(PERMISSION_CREATE)
                action = "create"
                user = manager.create(data)
                status = 201

            elif name == self.current_name:
                if "role" in data:
                    action = "update-role"
                    error  = "You cannot update your own role."
                    status = 403
                    self.send(action, name, None, error, status)
                    self.finish()
                    return

                self.authorize(PERMISSION_UPDATE_SELF)
                action = "update-self"
                user = manager.update(name, data)
            else:
                if "role" in data:
                    self.authorize(PERMISSION_UPDATE_ROLE)
                else:
                    self.authorize(PERMISSION_UPDATE_ALL)

                action = "update"
                user = manager.update(name, data)

        except UserNotFoundError as e:
            error = str(e)
            status = 404

        except Exception as e:
            error = str(e)
            status = 400

        self.send(action, name, user, error, status)


    def put(self, name):
        """
        PUT acts the same way as POST except it will raise an error when without
        a name.
        """
        logging.debug("==> PUT name %s" % name)
        if not name:
            self.send("update", name, None, "PUT requests need an username", 400)
            return

        return self.post(name)



manager = DataManager()
application = tornado.web.Application([
    (r"/",                      VersionHandler),
    (r"/me",                    MeHandler),
    (r"/users/([a-z0-9\-]*?)",  UsersHandler),
], default_handler_class=Custom404Handler)


if __name__ == "__main__":
    port = os.environ.get("PORT", 9000)
    application.listen(port)
    logging.warn("==> Starting application at http://localhost:%s" % port)
    tornado.ioloop.IOLoop.instance().start()
